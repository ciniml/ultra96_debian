.PHONY: all clean build-kernel build-dtbocfg build-rootfs

TARGETDIR := debian9-rootfs
DISTRO := stretch
ARCH := arm64
DEB_ARCH := aarch64
CROSS_COMPILE := /usr/bin/aarch64-linux-gnu-
EXTRAVERSION := xilinx-fpga
KERNEL_REVISION := 4.14.0-$(EXTVERSION)
KERNEL_IMAGE_DEB := linux-image-$(KERNEL_REVISION)_arm64.deb
KERNEL_HEADERS_DEB := linux-headers-$(KERNEL_REVISION)_arm64.deb

all: build-kernel build-rootfs

$(KERNEL_IMAGE_DEB) $(KERNEL_HEADERS_DEB): build-kernel

build-kernel:
	git clone --depth=1 -b xilinx-v2018.2 https://github.com/Xilinx/linux-xlnx/
	cp .config linux-xlnx/
	cd linux-xlnx/ && make deb-pkg -j8 ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILE) EXTRAVERSION=$(EXTRAVERSION) 2>&1 | tee $@.log

build-dtbocfg: build-kernel
	git clone --recursive --depth=1 -b v0.0.5 git://github.com/ikwzm/dtbocfg-ctrl-dpkg
	cd dtbocfg-ctrl-dpkg && make -f debian/rules binary ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILE) KERNEL_SRC_DIR=`pwd`/../linux-xlnx 2>&1 | tee $@.log

build-udmabuf: build-kernel
	git clone --recursive --depth=1 -b v1.3.2 git://github.com/ikwzm/udmabuf-kmod-dpkg
	cd udmabuf-kmod-dpkg && make -f debian/rules kernel_release=$(KERNEL_REVISION) arch=$(ARCH) deb_arch=$(DEB_ARCH) kernel_src_dir=`pwd`/../linux-xlnx 2>&1 | tee $@.log

build-rootfs: $(KERNEL_IMAGE_DEB) $(KERNEL_HEADERS_DEB) build-dtbocfg build-udmabuf
	mkdir $(TARGETDIR)
	debootstrap --arch=$(ARCH) --foreign $(DISTRO) $(TARGETDIR)
	cp /usr/bin/qemu-aarch64-static $(TARGETDIR)/usr/bin/
	cp /etc/resolv.conf $(TARGETDIR)/etc/
	cp $(KERNEL_IMAGE_DEB) $(TARGETDIR)/
	cp $(KERNEL_HEADERS_DEB)  $(TARGETDIR)/

	# Mount binfmt_misc to enable QEMU AARCH64 usermode emulation
	mount binfmt_misc -t binfmt_misc /proc/sys/fs/binfmt_misc/
	# Enable AARCH64 usermode emulation
	update-binfmts --enable qemu-aarch64
	#
	chroot $(TARGETDIR)
