FROM debian:latest
MAINTAINER Kenta IDA <fuga@fugafuga.org>

RUN apt-get update && apt-get upgrade -y \
    && apt-get install -y qemu-user-static debootstrap binfmt-support gcc-aarch64-linux-gnu build-essential libncurses-dev bc git

